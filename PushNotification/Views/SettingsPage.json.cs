using Starcounter;
using System;

namespace PushNotification.Views
{
    partial class SettingsPage : Json
    {

        void Handle(Input.Save action)
        {
            Console.WriteLine("saveme");

            this.Transaction.Commit();

            this.Message = "Changes saved";
        }
    }
}

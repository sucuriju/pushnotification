using Starcounter;
using System;
using System.Text;
using System.IO;
using PushNotification.Helpers;
using Newtonsoft.Json;
using System.Collections.Generic;
using PushNotification.Models;
using System.Linq;

namespace PushNotification.Views
{
    partial class MasterPageJson : Json
    {
        
        
        protected override void OnData()
        {
            
            base.OnData();
        }
       
        void Handle(Input.Endpoint Action)
        {
            // new/update 
            if (this.Endpoint.Length == 0 && Action.Value.Length > 0)
            {
                SubscriptionManager.UpdateSubscription(Action.Value);
            }
            // delete
            if (this.Endpoint.Length > 0 && Action.Value.Length == 0)
            {
                SubscriptionManager.DeleteSubscription(this.Endpoint);

            }

           
            this.Endpoint = Action.Value;
           
            
           
        }

        void Handle(Input.TestSendClick Action)
        {
            

            var sub = SubscriptionManager.SetMessage(this.Endpoint, this.Title, this.Message);
            this.EndpointMessage = sub.Push();

            //this.EndpointMessage = GCM.PushMozilla();
            //this.EndpointMessage = GCM.Push(new string[] { this.RegistrationId }, SettingsHelper.GetSettings().GCMAPIKEY);

            
        }
    }
}

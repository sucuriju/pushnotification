using Starcounter;
using PushNotification.Helpers;
using System.Linq;
using System.Collections.Generic;

namespace PushNotification.Views
{
    partial class BroadcastPage : Json
    {
        void Handle(Input.SendClick Action)
        {
            var subscriptions = SubscriptionManager.GetSubscriptions();
            
            foreach(var sub in subscriptions)
            {
                sub.SetMessage(this.Title, this.Message);
                sub.Push();
            }
            this.EndpointMessage = "Message broadcasted to " + subscriptions.Count + " subscriptions";



        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;
using PushNotification.Database;

namespace PushNotification.Helpers
{
    public class SubscriptionManager
    {
        public static void UpdateSubscription(string endpoint)
        {

            var m = GetSubscription(endpoint);
            Console.WriteLine("endpoint" + endpoint);

            Db.Transact(() =>
            {
                if (m == null)
                { 
                    new Subscription() { LifeSign = DateTime.Now, Endpoint = endpoint };
                }
                else
                {
                    m.Endpoint = endpoint;
                    m.LifeSign = DateTime.Now;
                }
                
            });
            
        }
        public static List<Subscription> GetSubscriptions()
        {
            return Db.SQL<Subscription>("SELECT n FROM Subscription n").ToList();
        }
        public static Subscription SetMessage(string endpoint, string messagetitle, string messagebody)
        {
            var m = GetSubscription(endpoint);
            return SetMessage(m, messagetitle, messagebody);
            
        }
        public static Subscription SetMessage(Subscription subscription, string messagetitle, string messagebody)
        {
            Db.Transact(() =>
            {
                subscription.MessageBody = messagebody;
                subscription.MessageTitle = messagetitle;
                subscription.MessageDate = DateTime.Now;
            });
            return subscription;
        }
        /*
        public static void SetMessage(string messagetitle, string messagebody)
        {
            var subscriptions = GetSubscriptions();
            foreach(var subscription in subscriptions)
            {
                SetMessage(subscription.Endpoint, messagetitle, messagebody);
            }
            
        }*/
        public static Subscription GetSubscription(string endpoint)
        {
            return Db.SQL<Subscription>("SELECT n FROM Subscription n WHERE n.Endpoint=?", endpoint).FirstOrDefault();
        }

        public static void DeleteSubscription(string endpoint)
        {

            var m = GetSubscription(endpoint);

            if (m != null) { 
                Db.Transact(() =>
                {
                    m.Delete();

                });
            };

        }

        public static string Broadcast(string messagetitle, string messagebody)
        {
            return Broadcast(GetSubscriptions(), messagetitle, messagebody);
        }
        public static string Broadcast(List<Subscription> subscriptions, string messagetitle, string messagebody)
        {
            return "TODO";
            /*
            var reg_ids = new List<string>();
            foreach (var subscription in subscriptions)
            {
                reg_ids.Add(subscription.RegistrationId);
                SubscriptionManager.SetMessage(subscription, messagetitle, messagebody);
            }

            return GCM.Push(reg_ids.ToArray(), SettingsHelper.GetSettings().GCMAPIKEY);
            */

        }
    }
}

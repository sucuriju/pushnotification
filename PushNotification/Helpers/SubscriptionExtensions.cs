﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushNotification.Database;

namespace PushNotification.Helpers
{
    public static class SubscriptionExtensions
    {

        public static string Push(this Subscription subscription)
        {
            return PushManager.Push(subscription.Endpoint);
            
        }
        public static void SetMessage(this Subscription subscription, string title, string message)
        {
            SubscriptionManager.SetMessage(subscription, title, message);
        }
    }
}

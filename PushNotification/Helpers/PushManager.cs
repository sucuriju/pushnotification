﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushNotification.Models;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using PushNotification.Database;

namespace PushNotification.Helpers
{
    public class PushManager
    {
        public static string Push(string endpoint)
        {
            var url = endpoint;
            var google = false;
            var registrationid = "";
            if (endpoint.StartsWith("https://android.googleapis.com/gcm/send")){
                google = true;
                url = "https://android.googleapis.com/gcm/send";
                registrationid = endpoint.Split('/')[endpoint.Split('/').Length - 1];
            }
            

            var wr = (HttpWebRequest)WebRequest.Create(url);

            wr.Method = "POST";
            wr.ContentType = "application/json";
            wr.Accept = "application/json";
            
            wr.Proxy = null;

            if (google)
            {
                wr.Headers["Authorization"] = "key = " + SettingsHelper.GetSettings().GCMAPIKEY;

                var pushmessage = new PushMessage() { registration_ids = new string[] { registrationid } };

                using (var s = wr.GetRequestStream())
                {
                    var data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(pushmessage));
                    s.Write(data, 0, data.Length);
                }
            }


            var resp = wr.GetResponse();

            using (var sr = new StreamReader(resp.GetResponseStream()))
            {
                return sr.ReadToEnd();
            }

        }

       

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace PushNotification.Helpers
{
    public class CookieHelper
    {
        public static string RegistrationIdCookie = "registrationid";
        public static int expiresdays = 30;
        public static Cookie GetRegistrationCookie()
        {
            List<Cookie> cookies = Handle.IncomingRequest.Cookies.Where(val => !string.IsNullOrEmpty(val)).Select(x => new Cookie(x)).ToList();
            Cookie cookie = cookies.FirstOrDefault(x => x.Name == RegistrationIdCookie);
            Console.WriteLine("cookie");
            Console.WriteLine(cookie);
            return cookie;
        }
        public static void SetRegistrationCookie(string value)
        {
            Cookie cookie = new Cookie()
            {
                Name = RegistrationIdCookie,
                Value = value,
                Expires = DateTime.UtcNow.AddDays(value.Length > 0 ? expiresdays : -1)
            };

            Handle.AddOutgoingCookie(cookie.Name, cookie.GetFullValueString());
        }
        public static void DeleteRegistrationCookie()
        {
            SetRegistrationCookie("");
            
        }

    }
}

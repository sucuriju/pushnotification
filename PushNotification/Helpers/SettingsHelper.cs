﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;
using PushNotification.Database;

namespace PushNotification.Helpers
{
    class SettingsHelper
    {
        public static Settings GetSettings()
        {
            
            var settings = Db.SQL<Settings>("SELECT n FROM Settings n").FirstOrDefault();
            if (settings == null)
            {
                return new Settings();
            }
            return settings;

        }
    }
}

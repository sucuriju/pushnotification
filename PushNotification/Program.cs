﻿using System;
using Starcounter;
using PushNotification.Views;
using PushNotification.Helpers;
using PushNotification.Database;
using System.Linq;

namespace PushNotification
{
    class Program
    {
        static void Main()
        {
   

            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            Handle.GET("/pushnotification", () => {
                Session.Ensure();

                return new MasterPageJson() {};
            });

            Handle.GET("/pushnotification/broadcast", () => {
                Session.Ensure();

                return new BroadcastPage() { };
            });

            Handle.GET("/pushnotification/manifest.json", () => {

                var manifest = new Manifest();
                manifest.gcm_sender_id = SettingsHelper.GetSettings().GCMSenderId;
                return new Response()
                {
           
                    Body = manifest.ToJson()
                };
                

            });
            Handle.GET("/pushnotification/message.json", () => {


                return new Response()
                {

                    Body = new MessageJson().ToJson()
                };


            });

            Handle.GET("/pushnotification/admin/settings", (Request request) =>
            {

                Session.Ensure();
                
                return Db.Scope(() =>
                {
                    return new SettingsPage
                    {
                        Data = SettingsHelper.GetSettings()
                    };
                    
                });
            });

            Handle.POST("/pushnotification/message.json", (Request request, SubscriptionJson subscriptionjson) => {

                
                //var endpointsplit = subscriptionjson.endpoint.Split('/');
                //var registrationid = endpointsplit[endpointsplit.Length - 1];


                var sub = SubscriptionManager.GetSubscription(subscriptionjson.endpoint);
                SubscriptionManager.UpdateSubscription(subscriptionjson.endpoint);

                var messagejson = new MessageJson() {
                    message = sub.MessageBody,
                    title = sub.MessageTitle
                };
                //update timestamp
                
                
                return new Response()
                {
                    Body = messagejson.ToJson()
                };


            });
        }

        

    }
}
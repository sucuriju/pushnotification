﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace PushNotification.Database
{
    [Database]
    public class Subscription
    {
        public string Endpoint { get; set; }
        public DateTime LifeSign { get; set; }
        public string MessageBody { get; set; }
        public string MessageTitle { get; set; }
        public DateTime MessageDate { get; set; }

    }
    
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace PushNotification.Database
{
    [Database]
    public class Settings
    {
        public string GCMAPIKEY { get; set; }
        public string GCMSenderId { get; set; }

    }
    
}
